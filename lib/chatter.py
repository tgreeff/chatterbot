from queue import Queue
from pynput.keyboard import Key, Controller
import time
import random

class Chatter:
    def __init__(self, settings, str_len=3, time_len=3):
        self.settings = settings
        self.string_history = Queue(str_len)
        self.time_history = Queue(time_len)
        self.keyboard = Controller()
        
        if("seed" in settings.keys()):
            if(settings["seed"] != ""):
                self.seed = random.seed(settings["seed"], version=2)
                
        if(self.seed == None):
            self.seed = random.seed()
            
        self.random = random.Random(self.seed)
        
    
    def type_random_word(self):
        word = self.get_random_word()
        if(not self.string_history.empty()):
            length = self.string_history.qsize()
            while(word in [self.string_history.queue[0], self.string_history.queue[length - 1]]):
                word =  self.get_random_word()
            
        self.keyboard.type(word)
        print(word)
        
        # Add word to history
        if(self.string_history.full()):
            self.string_history.get()
        self.string_history.put(word)
         
        # Send message
        self.keyboard.press(Key.enter)
        self.keyboard.release(Key.enter)
        
    
    def wait_random(self):
        wait_value =  self.get_random_time()
        if(not self.time_history.empty()):
            while(wait_value != self.time_history.queue[0]):
                wait_value =  self.get_random_time()
        
        # Add time to history
        if(self.time_history.full()):
            self.time_history.get()
        self.time_history.put(wait_value)

        # Setup time to match set wait time
        if(self.settings["minutes"]):
            wait_value *= 60

        for x in range(wait_value, 0, -1):
            if(x > 60):
                if(x % 60 == 0):
                    print("Time Left: {:2.0f} minutes".format(x/60), end="\r")
                    
            else:
                print("Time Left: {:2} seconds".format(x), end="\r")
                
            time.sleep(1) 
        print("{:50}".format(""), end="\r")    
        
    
    
    def get_random_word(self):
        index = self.random.randint(0, len(self.settings["word_list"]) - 1)
        return self.settings["word_list"][index]
        
    
    def get_random_time(self):
        return self.random.randint(1, self.settings["max_input_delay"] - 1)