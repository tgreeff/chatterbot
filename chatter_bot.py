"""
    Chatter-Bot script
    
    Author: Taylor Greeff
    Dependencies: pyyaml, pynput, argparse

"""

import sys
import os
import yaml
import argparse
import time
   
from lib.chatter import Chatter

def main(argd):
    settings = {}
    if(os.path.exists(argd["file"])):
        with open(argd["file"]) as file:
            settings = yaml.load(file, Loader=yaml.FullLoader)
    
    settings["max_input_delay"] = int(settings["max_input_delay"])
    
    for x in range(5, -1, -1):
        print("Starting in {}".format(x + 1), end="\r")
        time.sleep(1)
    print()
    
    chatter = Chatter(settings)
    while(True):
        chatter.type_random_word()
        chatter.wait_random()

if __name__ == "__main__":
    # Create arg parser
    parser = argparse.ArgumentParser(description="Arguments for the api setup")
    parser.add_argument("-f", "--file", default="settings.yml", help="Details the list of words to type, and the presets.")

    # Parse args
    args = parser.parse_known_args()
    argd = args[0].__dict__
    
    main(argd)